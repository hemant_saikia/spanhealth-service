package main

import (
	"fmt"
	"log"
	"net/http"
	"github.com/gorilla/mux"
	"bitbucket.org/hemant_saikia/spanhealth-service/configmanager"
)


func newRouter() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/h", handler).Methods("GET")

	r.HandleFunc("/getSpan", GetSpanHealth).Methods("GET")

	return r
}

func main() {
	if err := configmanager.InitConfig("config.json"); err != nil {
		log.Fatalf("Error while initializing the config. Error: [%#v]", err)
		//panic(1)
	}
	go InitSpanHealth()
	
	// The "HandleFunc" method accepts a path and a function as arguments
	r := newRouter()

	// and the handler defined above (in "HandleFunc") is used
	http.ListenAndServe(":9911", r)
}

func handler(w http.ResponseWriter, r *http.Request) {
	// For this case, we will always pipe "Hello World" into the response writer
	fmt.Fprintf(w, "Hello World!")
}