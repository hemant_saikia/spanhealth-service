package configmanager

import (
	"encoding/json"
	"io/ioutil"

	// "bitbucket.org/yellowmessenger/asterisk-ari/metrics"
	// "bitbucket.org/yellowmessenger/asterisk-ari/queuemanager"
	// "bitbucket.org/yellowmessenger/asterisk-ari/ymlogger"
)

type appconfig struct {
	// LoggerConf                  ymlogger.LoggerConf              `json:"logger_conf"`
	// MetricsConf                 metrics.Config                   `json:"metrics_conf"`
	// QueueConnParams             queuemanager.QueueConnParams     `json:"queue_conn_params"`
	// QueueListenerParams         queuemanager.QueueListenerParams `json:"queue_listener_params"`
	// QueueMessageParams          queuemanager.QueueMessageParams  `json:"queue_message_params"`
	MySQLUser                   string                           `json:"mysql_user"`
	MySQLPassword               string                           `json:"mysql_password"`
	MySQLDB                     string                           `json:"mysql_db"`
	ARIApplication              string                           `json:"ari_application"`
	ARIUsername                 string                           `json:"ari_username"`
	ARIPassword                 string                           `json:"ari_password"`
	ARIURL                      string                           `json:"ari_url"`
	APIUsername                 string                           `json:"api_username"`
	APIPassword                 string                           `json:"api_password"`
	ARIAPIRetry                 int                              `json:"ari_api_retry"`
	ARIWebsocketURL             string                           `json:"ari_websocket_url"`
	SIPIP                       string                           `json:"sip_ip"`
	CallAPIRequestsPerSecond    int                              `json:"call_api_requests_per_second"`
	TTSFilePath                 string                           `json:"tts_file_path"`
	TTSFrequency                int                              `json:"tts_frequency"`
	TTSFileOutputFormat         string                           `json:"tts_file_output_format"`
	DefaultWelcomeFile          string                           `json:"default_welcome_file"`
	TTSVoiceID                  string                           `json:"tts_voice_id"`
	STTSampleRate               int32                            `json:"stt_sample_rate"`
	STTLanguage                 string                           `json:"stt_language"`
	STTType                     string                           `json:"stt_type"`
	STTStreamBufferSize         int                              `json:"stt_stream_buffer_size"`
	BotEndPoint                 string                           `json:"bot_endpoint"`
	AzureTokenEndpoint          string                           `json:"azure_token_endpoint"`
	AzureTTSAPIKey              string                           `json:"azure_tts_api_key"`
	AzureTTSEndpoint            string                           `json:"azure_tts_endpoint"`
	AzureSTTAPIKey              string                           `json:"azure_stt_api_key"`
	AzureSTTEndpoint            string                           `json:"azure_stt_endpoint"`
	AzureSpeakerAPIEndpoint     string                           `json:"azure_speaker_api_endpoint"`
	AzureSpeakerAPIKey          string                           `json:"azure_speaker_api_key"`
	RecordingTerminationKey     string                           `json:"recording_termination_key"`
	RecordingMaxSilence         int                              `json:"recording_max_silence"`
	RecordingMaxDuration        int                              `json:"recording_max_duration"`
	RecordingDirectory          string                           `json:"recording_directory"`
	RecordingFormat             string                           `json:"recording_format"`
	CallRecordingFormat         string                           `json:"call_recording_format"`
	AzureRecordingContainerName string                           `json:"azure_recording_container_name"`
	CallbackMaxTries            int                              `json:"callback_max_tries"`
	InboundCallbackURL          string                           `json:"inbound_callback_url"`
	ContinuousDTMFDelay         int                              `json:"continuous_dtmf_delay"`
	PipeHealthDelay             int                              `json:"pipehealth_delay"`
	CampaignDelayPerCallMS      int                              `json:"campaign_delay_per_call_ms"`
	CountryCode                 string                           `json:"country_code"`
	RegionCode                  string                           `json:"region_code"`
	ExotelAPIKey                string                           `json:"exotel_api_key"`
	ExotelAPIToken              string                           `json:"exotel_api_token"`
	ExotelAccountSID            string                           `json:"exotel_account_sid"`
}

// ConfStore stores the configuration variables
var ConfStore *appconfig

// InitConfig initializes the config
func InitConfig(
	fileName string,
) error {
	data, err := ioutil.ReadFile(fileName)
	if err != nil {
		return err
	}
	if err = json.Unmarshal([]byte(data), &ConfStore); err != nil {
		return err
	}
	return nil
}
